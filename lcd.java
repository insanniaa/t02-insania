public class lcd {
    public static void main(String[] args) throws Exception {

        LcdLayar lcd1 = new LcdLayar();
        lcd1.turnOff();
        lcd1.turnOn();
        lcd1.cableDown();
        lcd1.cableUp();
        lcd1.setVolume(50);
        lcd1.setStatus("Menyala");
        lcd1.setBrightness(20);
        lcd1.setCable("HDMI");
        lcd1.displayMessage();

    }
}
